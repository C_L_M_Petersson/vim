# Vimrc

## Purpose

My .vimrc was unorganized, sloppy, and largely copied from another .vimrc before I knew the first thing about viml. After completing a vimscript tutorial I decided to rebuild it from the ground up. I'm writing this repository this not only because I want to have an efficient .vimrc, but also because I want to know exactly how my Vim configuration functions and because I enjoy it. This will likely be reflected in the code, as much of it may fall under the category of reinventing the wheel.

## Makefile

* `install` Installs the latest version of every file.
* `force` Same as `install`, but will reset any changes made to the files.
* `reset` Resets any changes made to the files.
* `update` Updates the non-submodule files.
* `submodule` Updates the submodules.
* `nvim` Uses symbolic links to allow nvim to use the configuration files.
* `clean` Cleans the directory of configuration files.
