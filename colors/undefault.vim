set background=dark
set t_Co=256
let g:colors_name = "undefault"

function! s:ColorParser(colordict)
    for [group, settings] in items(a:colordict)
        execute 'highlight ' . group .
            \' ctermfg=' . settings[0] .
            \' ctermbg=' . settings[1] .
            \' cterm='   . settings[2]
    endfor
endfunction

" Colors {{{
call s:ColorParser({
    \'Normal        ' : [   231,     16, 'NONE'     ],
    \
    \'VertSplit     ' : [   231,     16, 'NONE'     ],
    \'StatusLineNC  ' : [   231,     16, 'UNDERLINE'],
    \
    \'Comment       ' : [     8, 'NONE', 'NONE'     ],
    \'Folded        ' : [     8, 'NONE', 'NONE'     ],
    \
    \'Constant      ' : [   228, 'NONE', 'NONE'     ],
    \'String        ' : [   228, 'NONE', 'NONE'     ],
    \'Character     ' : [   228, 'NONE', 'NONE'     ],
    \'Number        ' : [   228, 'NONE', 'NONE'     ],
    \'Boolean       ' : [   228, 'NONE', 'NONE'     ],
    \'Float         ' : [   228, 'NONE', 'NONE'     ],
    \
    \'Identifier    ' : [   120, 'NONE', 'NONE'     ],
    \'Function      ' : [   120, 'NONE', 'NONE'     ],
    \
    \'Statement     ' : [   204, 'NONE', 'NONE'     ],
    \'Conditional   ' : [   204, 'NONE', 'NONE'     ],
    \'Repeat        ' : [   204, 'NONE', 'NONE'     ],
    \'Label         ' : [   204, 'NONE', 'NONE'     ],
    \'Operator      ' : [   204, 'NONE', 'NONE'     ],
    \'Keyword       ' : [   204, 'NONE', 'NONE'     ],
    \'Exception     ' : [   204, 'NONE', 'NONE'     ],
    \
    \'PreProc       ' : [   205, 'NONE', 'NONE'     ],
    \'Include       ' : [   205, 'NONE', 'NONE'     ],
    \'Define        ' : [   205, 'NONE', 'NONE'     ],
    \'Macro         ' : [   205, 'NONE', 'NONE'     ],
    \'PreCondit     ' : [   205, 'NONE', 'NONE'     ],
    \
    \'Type          ' : [    35, 'NONE', 'NONE'     ],
    \'StorageClass  ' : [    35, 'NONE', 'NONE'     ],
    \'Structure     ' : [    35, 'NONE', 'NONE'     ],
    \'Typedef       ' : [    35, 'NONE', 'NONE'     ],
    \
    \'Special       ' : ['NONE', 'NONE', 'NONE'     ],
    \'SpecialChar   ' : ['NONE', 'NONE', 'NONE'     ],
    \'Tag           ' : ['NONE', 'NONE', 'NONE'     ],
    \'Delimiter     ' : ['NONE', 'NONE', 'NONE'     ],
    \'SpecialComment' : ['NONE', 'NONE', 'NONE'     ],
    \'Debug         ' : ['NONE', 'NONE', 'NONE'     ],
    \
    \'DiffAdd       ' : ['NONE',     55, 'NONE'     ],
    \'DiffChange    ' : ['NONE',      9, 'NONE'     ],
    \'DiffDelete    ' : ['NONE',      8, 'NONE'     ],
    \'DiffText      ' : ['NONE',     52, 'NONE'     ],
    \
    \'Underlined    ' : [    12, 'NONE', 'NONE'     ],
    \
    \'Ignore        ' : ['NONE', 'NONE', 'NONE'     ],
    \
    \'Error         ' : [    16,      9, 'BOLD'     ],
    \
    \'Todo          ' : [    16,     11, 'BOLD'     ],
    \ })
" }}}
