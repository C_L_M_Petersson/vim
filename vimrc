" General Settings {{{
syntax on

filetype plugin indent on

set shiftwidth=4
set tabstop=4
set expandtab

set incsearch
set hlsearch

set wildmode=longest,list,full
set wildmenu

set ignorecase
set smartcase

set splitbelow
set splitright
set fillchars+=vert:│
hi VertSplit ctermbg=NONE guibg=NONE

colorscheme undefault

set backupdir=.
set directory=.

set foldmethod=marker

set nrformats-=octal
" }}}

" Unwanted whitespaces, tabs, long lines {{{
exec "set listchars=tab:\uBB\uB7,trail:\uB7"
set list

augroup buf
    autocmd BufEnter * :let w:m1=matchadd('ErrorMsg', '\v%82v', -1)
    autocmd BufWrite * :call DeleteTrailingWS()
augroup END

func! DeleteTrailingWS()
    if (index(["vimrc", "init.vim"], expand('%:t')) < 0)
        exe "normal mz"
        %s/\s\+$//ge
        exe "normal `z"
    endif
endfunc
" }}}

" Status line {{{
set statusline=%.20f " File name
set statusline+=%m   " Modified flag
set statusline+=%r   " Readonly flag
set statusline+=%=   " Switch alignment
set statusline+=[
set statusline+=%l   " Current line
set statusline+=/
set statusline+=%L   " Total lines
set statusline+=]
" }}}

" Pathogen {{{
" Allows for easy plugin management.
runtime bundle/pathogen/autoload/pathogen.vim
execute pathogen#infect()
" }}}

" Key bindings {{{

" General {{{
noremap  <c-f> <c-c>
noremap! <c-f> <c-c>

set pastetoggle=<F2>

noremap Q <nop>

noremap <space> za

let mapleader      = "'"
let maplocalleader = ","

nnoremap <leader>ve :split  $MYVIMRC<cr>
nnoremap <leader>vs :source $MYVIMRC<cr>

" }}}

" External programs {{{

" make {{{
"nnoremap <leader>ms  :execute ':silent !make > /dev/null'<CR> | execute ':redraw'<CR>
"nnoremap <leader>ms  :silent exec "!make > /dev/null &"<CR>
"nnoremap <leader>ms  :silent exec "!make 1>&2 2>/dev/null &"<CR>
nnoremap <leader>mv  :!make<CR>
nnoremap <leader>Mv  :!make 
" }}}

" git {{{
nnoremap <leader>G :!git 
" }}}

" }}}

" Operator-Pending Mappings {{{
" Brackets and citation marks
onoremap in(  :<c-u>normal! f(vi(<cr>
onoremap in[  :<c-u>normal! f[vi[<cr>
onoremap in{  :<c-u>normal! f{vi{<cr>
onoremap in\< :<c-u>normal! f\<vi\<<cr>
onoremap in"  :<c-u>normal! f"vi"<cr>
onoremap in'  :<c-u>normal! f'vi'<cr>

onoremap an(  :<c-u>normal! f(va(<cr>
onoremap an[  :<c-u>normal! f[va[<cr>
onoremap an{  :<c-u>normal! f{va{<cr>
onoremap an\< :<c-u>normal! f\<va\<<cr>
onoremap an"  :<c-u>normal! f"va"<cr>
onoremap an'  :<c-u>normal! f'va'<cr>

onoremap il(  :<c-u>normal! F)vi(<cr>
onoremap il[  :<c-u>normal! F]vi[<cr>
onoremap il{  :<c-u>normal! F}vi{<cr>
onoremap il\< :<c-u>normal! F\>vi\<<cr>
onoremap il"  :<c-u>normal! F"vi"<cr>
onoremap il'  :<c-u>normal! F'vi'<cr>

onoremap al(  :<c-u>normal! F)va(<cr>
onoremap al[  :<c-u>normal! F]va[<cr>
onoremap al{  :<c-u>normal! F}va{<cr>
onoremap al\< :<c-u>normal! F\>va\<<cr>
onoremap al"  :<c-u>normal! F"va"<cr>
onoremap al'  :<c-u>normal! F'va'<cr>

" E-mail
onoremap in@ :<c-u>execute "normal! /@\r3evB"<cr>
onoremap il@ :<c-u>execute "normal! ?@\r3evB"<cr>
" }}}

" Movement {{{
noremap H ^
noremap J L
noremap K H
noremap L $

noremap <c-j> Lzz
noremap <c-k> Hzz

cnoremap <m-h> <LEFT>
cnoremap <m-j> <DOWN>
cnoremap <m-k> <UP>
cnoremap <m-l> <RIGHT>
" }}}

" NOPing bad habits {{{
noremap  <UP>    <nop>
noremap  <LEFT>  <nop>
noremap  <RIGHT> <nop>
noremap  <DOWN>  <nop>
noremap  <BS>    <nop>

noremap! <UP>    <nop>
noremap! <LEFT>  <nop>
noremap! <RIGHT> <nop>
noremap! <DOWN>  <nop>
noremap! <BS>    <nop>
" }}}

" Command and search mode {{{
noremap / /\v
noremap ? ?\v

nnoremap <leader>s :%s/\v
vnoremap <leader>s :%s/\v

nnoremap <leader>g :%g/\v
vnoremap <leader>g :%g/\v

nnoremap <leader>e :e %:h/
" }}}

" }}}

" Specific to nvim {{{
if has("nvim")
    set mouse=
endif
" }}}

" Specific to vim {{{
if 1-has("nvim")
    set nocompatible
endif
" }}}
